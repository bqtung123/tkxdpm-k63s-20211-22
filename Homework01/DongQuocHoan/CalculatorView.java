/**
 * Copyright(C) 2021 Luvina Software
 * Customer.java, Jul 27, 2021, HoanDQ
 */
package javaswing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Label;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
/**
 * Tạo ra 1 máy tính bỏ túi bằng java Swing
 * @author Hoan
 */
public class CalculatorView {

	private JFrame frame;
	private JTextField textDisplay;
	private double firstNumber;
	private double secondNumber;
	private double result;
	private String operator;
	private String answer;

	/**
	 * Launch the calculator application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorView window = new CalculatorView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculatorView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Set 1 Frame với absolutely Layout để tạo 2 panel
		frame = new JFrame();
		frame.setBounds(100, 100, 390, 512);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		
		//Tạo 1 Panel để chứa tên hãng và màn hình Display
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 372, 124);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblMyCalculator = new JLabel("Hoan's Calculator");
		lblMyCalculator.setBounds(243, 0, 119, 31);
		panel.add(lblMyCalculator);
		
		textDisplay = new JTextField();
		textDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
		textDisplay.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textDisplay.setBounds(10, 41, 352, 73);
		panel.add(textDisplay);
		textDisplay.setColumns(10);
		
		//Tạo 1 Panel để chứa các nút bấm
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 134, 356, 337);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(0, 4, 0, 0));
		
		//Nút xóa 1 kí tự
		JButton btnDeleteOneChar = new JButton("\u2190");
		btnDeleteOneChar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String backspace = null;
				if(textDisplay.getText().length() > 0) {
					StringBuilder str = new StringBuilder(textDisplay.getText());
				    str.deleteCharAt(textDisplay.getText().length() - 1);
				    backspace = str.toString();
				    textDisplay.setText(backspace);
				}
			}
		});
		btnDeleteOneChar.setFont(new Font("Tahoma", Font.BOLD, 25));
		panel_1.add(btnDeleteOneChar);
		
		//Nút xóa hết
		JButton btnDeleteAll = new JButton("C");
		btnDeleteAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textDisplay.setText(null);
			}
		});
		btnDeleteAll.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnDeleteAll);
		
		//Nút chia lấy dư
		JButton btnMod = new JButton("%");
		btnMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNumber = Double.parseDouble(textDisplay.getText());
			    textDisplay.setText("");
				operator ="%";
			}
		});
		btnMod.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnMod);
		
		//Nút cộng
		JButton btnAdd = new JButton("+");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNumber = Double.parseDouble(textDisplay.getText());
			    textDisplay.setText("");
				operator ="+";
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnAdd);
		
		//Nút 7
		JButton btnSeven = new JButton("7");
		btnSeven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnSeven.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnSeven.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnSeven);
		
		//Nút 8
		JButton btnEight = new JButton("8");
		btnEight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnEight.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnEight.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnEight);
		
		//Nút 9
		JButton btnNine = new JButton("9");
		btnNine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnNine.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnNine.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnNine);
		
		//Nút trừ
		JButton btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNumber = Double.parseDouble(textDisplay.getText());
			    textDisplay.setText("");
				operator ="-";
			}
		});
		btnMinus.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnMinus);
		
		//Nút 4
		JButton btnFour = new JButton("4");
		btnFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnFour.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnFour.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnFour);
		//Nút 5
		JButton btnFive = new JButton("5");
		btnFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnFive.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnFive.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnFive);
		//Nút 6
		JButton btnSix = new JButton("6");
		btnSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnSix.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnSix.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnSix);
		
		//Nút nhân
		JButton btnMultiple = new JButton("*");
		btnMultiple.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNumber = Double.parseDouble(textDisplay.getText());
			    textDisplay.setText("");
				operator ="*";
			}
		});
		btnMultiple.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnMultiple);
		
		//Nút 1
		JButton btnOne = new JButton("1");
		btnOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnOne.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnOne.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnOne);
		
		//Nút 2
		JButton btnTow = new JButton("2");
		btnTow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnTow.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnTow.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnTow);
		
		//Nút 3
		JButton btnThree = new JButton("3");
		btnThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnThree.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnThree.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnThree);
		
		//Nút chia
		JButton btnDevide = new JButton("/");
		btnDevide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNumber = Double.parseDouble(textDisplay.getText());
			    textDisplay.setText("");
				operator ="/";
			}
		});
		btnDevide.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnDevide);
		
		//Nút 0
		JButton btnZero = new JButton("0");
		btnZero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnZero.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnZero.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnZero);
		
		//Nút .
		JButton btnDot = new JButton(".");
		btnDot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eventNumber = textDisplay.getText() + btnDot.getText();
				textDisplay.setText(eventNumber);
			}
		});
		btnDot.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnDot);
		
		//Nút lấy kết quả tính tiếp
		JButton btnAnds = new JButton("Ans");
		btnAnds.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String t =	textDisplay.getText();
				firstNumber = Double.parseDouble(t);
				textDisplay.setText(t);
			}
		});
		btnAnds.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnAnds);
		
		//Nút =
		JButton btnNewButton_19 = new JButton("=");
		btnNewButton_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String answer;
				secondNumber = Double.parseDouble(textDisplay.getText());
				if(operator == "+") {
					result = firstNumber + secondNumber;
					answer = String.format("%.0f", result);
					textDisplay.setText(answer);
				} else if (operator == "-") {
					result = firstNumber - secondNumber;
					answer = String.format("%.0f", result);
					textDisplay.setText(answer);
				} else if (operator == "*") {
					result = firstNumber * secondNumber;
					answer = String.format("%.0f", result);
					textDisplay.setText(answer);
				} else if (operator == "/") {
					if(secondNumber == 0) {
						textDisplay.setText("Error!");
					} else {
					result = firstNumber / secondNumber;
					answer = String.format("%.4f", result);
					textDisplay.setText(answer); 
					}
				} else if (operator == "%") {
					result = firstNumber % secondNumber;
					answer = String.format("%.0f", result);
					textDisplay.setText(answer);
				} 
			}
		});
		btnNewButton_19.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel_1.add(btnNewButton_19);
	}
}
