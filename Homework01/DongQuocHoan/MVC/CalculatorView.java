/**
 * Copyright(C) 2021 Luvina Software
 * CalculatorControl.java, Jul 29, 2021, HoanDQ
 */
package javaswingbaitap;

import java.awt.Color;
import java.awt.Label;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CalculatorView {

	private JFrame frame;
	private String lblValueResult;
	private String lblshowInformation;
	/**
	 * Create the application.
	 */
	public CalculatorView() {
		frame = new JFrame();
		initialize();
		frame.setVisible(true);
		frame.setResizable(false);
	}
    
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFisrtNumber = new JLabel("Nhập số thứ nhất :");
		lblFisrtNumber.setBounds(42, 38, 111, 22);
		frame.getContentPane().add(lblFisrtNumber);
		
		JLabel lblSecondNumber = new JLabel("Nhập số thứ hai :");
		lblSecondNumber.setBounds(42, 112, 111, 21);
		frame.getContentPane().add(lblSecondNumber);
		
		//TextField nhập số thứ nhất
		JTextField txtFirstNumber = new JTextField();
		txtFirstNumber.setBounds(163, 40, 96, 19);
		frame.getContentPane().add(txtFirstNumber);
		txtFirstNumber.setColumns(10);
		
		//TextField nhập số thứ 2
		JTextField txtSecondNumber = new JTextField();
		txtSecondNumber.setBounds(163, 114, 96, 19);
		frame.getContentPane().add(txtSecondNumber);
		txtSecondNumber.setColumns(10);
		
		JLabel lblResult = new JLabel("Kết quả :");
		lblResult.setBounds(42, 195, 59, 13);
		frame.getContentPane().add(lblResult);
		
		Label lblOperator = new Label("Operator : ");
		lblOperator.setBounds(42, 73, 59, 21);
		frame.getContentPane().add(lblOperator);
		
		//Combobox thay đổi phép tính
		JComboBox comboBoxOperator = new JComboBox();
		comboBoxOperator.setModel(new DefaultComboBoxModel(new String[] {"+", "-", "*", "/", "%"}));
		comboBoxOperator.setBounds(163, 73, 59, 22);
		frame.getContentPane().add(comboBoxOperator);
		
		//Label in ra thông tin lỗi
		Label lblInformation = new Label("");
		lblInformation.setForeground(Color.BLACK);
		lblInformation.setBackground(Color.WHITE);
		lblInformation.setBounds(163, 151, 212, 21);
		frame.getContentPane().add(lblInformation);
		
		//Label in ra kết quả
		JLabel lblPrintResult = new JLabel("");
		lblPrintResult.setBackground(Color.WHITE);
		lblPrintResult.setForeground(Color.BLACK);
		lblPrintResult.setBounds(163, 195, 96, 13);
		frame.getContentPane().add(lblPrintResult);
		
		
		//Button tính để xử lí sự kiện
		JButton btnCalculator = new JButton("Tính");
		btnCalculator.setBounds(42, 151, 85, 21);
		frame.getContentPane().add(btnCalculator);
		
			CalculatorControl calculatorControl = new CalculatorControl(txtFirstNumber,txtSecondNumber,lblInformation,lblPrintResult,comboBoxOperator);
			btnCalculator.addActionListener(calculatorControl);
			lblInformation =  calculatorControl.getLblshowInformation();
			lblPrintResult = calculatorControl.getLblValueResult();
			lblInformation.setText(lblshowInformation);
			lblPrintResult.setText(lblValueResult);
			}
}
