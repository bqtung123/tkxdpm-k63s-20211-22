/**
 * Copyright(C) 2021 Luvina Software
 * CalculatorModel.java, Jul 29, 2021, HoanDQ
 */
package javaswingbaitap;

/**
 * Xử lí các Logic của các số
 * @author Hoan
 */
public class CalculatorModel {
	/**
	 * Phương thức khởi tạo
	 */
	public CalculatorModel() {
		super();
	}
	/*
	 * Hàm kiểm tra số thứ 2 có khác 0 không
	 */
	public boolean checkLogicNumber2(Double secondNumber) {
		  if(secondNumber == 0) {
			return false;
		}
		return true;
	}
	/*
	 * Hàm kiểm tra xem phần nhập có phải số không
	 */
   public boolean isNumeric(String number) {
	   try {
		   Double.parseDouble(number);
		   return true;
	   } catch(NumberFormatException e) {
		   return false;
	   }
   }
}