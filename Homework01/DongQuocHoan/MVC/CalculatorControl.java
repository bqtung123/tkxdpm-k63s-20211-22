/**
 * Copyright(C) 2021 Luvina Software
 * CalculatorControl.java, Jul 29, 2021, HoanDQ
 */
package javaswingbaitap;

import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
/**
 * 
 * @author Hoan
 */
public class CalculatorControl implements ActionListener{
	private JTextField txtNumber1;
	private JTextField txtNumber2;
	private String cbbOperator;
	private Label lblshowInformation;
	private JLabel lblValueResult;
	private JComboBox<String> comboboxOperator;
	
	

	/**
	 * Phương thức khởi tạo truyền tham số
	 * @param txtNumber1
	 * @param txtNumber2
	 * @param lblshowInformation
	 * @param lblValueResult
	 * @param comboboxOperator
	 */
	public CalculatorControl(JTextField txtNumber1, JTextField txtNumber2, Label lblshowInformation,
			JLabel lblValueResult, JComboBox<String> comboboxOperator) {
		super();
		this.txtNumber1 = txtNumber1;
		this.txtNumber2 = txtNumber2;
		this.lblshowInformation = lblshowInformation;
		this.lblValueResult = lblValueResult;
		this.comboboxOperator = comboboxOperator;
	}

    /*
     * Phương thức xử lí sự kiện sau khi ấn button Tính
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Khởi tạo đối tượng check Logic
		CalculatorModel model = new CalculatorModel();
		Double number1 = null;
		Double number2 = null;
		Double result = null;
		String answer = "";
	    lblshowInformation.setText("");
	    lblValueResult.setText("");
	    //Kiểm tra 2 số nhập có phải là số không
		if(model.isNumeric(txtNumber1.getText())) {
			number1 = Double.parseDouble(txtNumber1.getText());
		} else {
			lblshowInformation.setText("Number khong phai so!");
		}
		if(model.isNumeric(txtNumber2.getText())) {
			number2 = Double.parseDouble(txtNumber2.getText());
		} else {
			lblshowInformation.setText("Number khong phai so!");
		}
		//Lấy dấu được chọn
		if(comboboxOperator.getSelectedIndex() != -1) {
			cbbOperator = comboboxOperator.getItemAt(comboboxOperator.getSelectedIndex());
		}
		//Xử lí phép tính với mỗi dấu được chọn
		if (cbbOperator == "+") {
			result = number1 + number2;
			answer = String.format("%.2f", result);
		} else if (cbbOperator == "-") {
			result = number1 - number2;
			answer = String.format("%.2f", result);
		} else if (cbbOperator == "*") {
			result = number1 * number2;
			answer = String.format("%.2f", result);
		}else if (cbbOperator == "/") {
			//Kiểm tra số thứ 2 phải khác 0 trong phép chia
			if (model.checkLogicNumber2(number2)) {
				result = number1 / number2;
				answer = String.format("%.2f", result);
			} else {
				lblshowInformation.setText("Number 2 phai khac 0");
			}
		}
		else if (cbbOperator == "%") {
			//Kiểm tra số thứ 2 phải khác 0 trong phép mod
			if (model.checkLogicNumber2(number2)) {
				result = number1 % number2;
				answer = String.format("%.2f", result);
			} else {
				lblshowInformation.setText("Number 2 phai khac 0");
			}
		}
	   lblValueResult.setText(answer);
	}
	/**
	 * @return the lblshowInformation
	 */
	public Label getLblshowInformation() {
		return lblshowInformation;
	}



	/**
	 * @return the lblValueResult
	 */
	public JLabel getLblValueResult() {
		return lblValueResult;
	}
}
