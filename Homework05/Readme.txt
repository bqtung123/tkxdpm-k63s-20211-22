Bài tập cá nhân: Vẽ các biểu đồ trình tự cho các lớp thiết kế trong use case mình phụ trách. Có 
thể cần vẽ nhiều biểu đồ, mỗi biểu đồ ứng với một scenario trong use case. Dựa trên các biểu 
đồ trình tự đã vẽ, vẽ biểu đồ lớp thiết kế cho use case mình phụ trách.
Bài tập nhóm: Gộp lại các biểu đồ lớp thiết kế của mỗi thành viên, tổ chức thành các package 
cho hợp lý, thống nhất cách thức đặt tên.
Sau đó, vẽ biểu đồ package cho toàn nhóm và cho từng cá nhân (cần phân thành các tầng)