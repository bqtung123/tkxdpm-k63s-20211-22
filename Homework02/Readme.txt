I.Phân công chức năng:
Bùi Quang Tùng: thuê xe
Nguyễn Đức Tân: tìm kiếm
Đồng Quốc Hoàn: trả xe
Thoeun Rathana: thêm bãi xe mới (trong chức năng quản lý thông tin bãi xe)
Nguyễn Đức Trí: chỉnh sửa thông tin bãi xe(trong chức năng quản lý thông tin bãi xe)
Mai Tuấn Hưng: thêm xe mới (trong quản lý các xe đang sử dụng)

II. Phân công công việc tuần này:
a. Bài tập cá nhân:
-Mỗi thành viên sẽ đặc tả usecase, đặc tả usecase ứng với chức năng đã được phân công
b. Bài tập nhóm:
-Bùi Quang Tùng: vẽ biểu đồ usecase tổng quan, phân rã chức năng thuê xe
-Nguyễn Đức Tân: vẽ biểu đồ usecase phân rã chức năng tìm kiếm bãi xe
-Đồng Quốc Hoàn: vẽ biểu đồ usecase phân rã chức năng trả xe
-Thoeun Rathana:  vẽ biểu đồ usecase phân rã chức năng quản lý thông tin bãi xe
-Mai Tuấn Hưng: vẽ biểu đồ usecase phân rã chức năng quản lý các xe đang sử dụng.